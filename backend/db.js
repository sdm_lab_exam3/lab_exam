const mysql = require ('mysql')

const pool = mysql.createPool ({
    host:'mydb',
    user:'root',
    password:'root',
    database:'mydb',
    waitForConnections:'true',
    connectionlimit: 10,
    quelimit: 0
});

module.exports = pool