const { request, response } = require('express')
const express = require('express')

const db = require('../db')

const utils = require('../utils')

const router = express.Router()

router.post('/', (request,response) => {
    const{ movie_title, movie_release_date,movie_time,director_name} = request.body

    const query = `
    INSERT INTO movie
    (movie_title, movie_release_date,movie_time,director_name)
    VALUES
    { '${movie_title}', '${movie_release_date}', '${movie_time}', '${director_name}'}
    `
    
})

router.get('/', (request,response) => {

    const query = `
    SELECT * FROM movie
    `

})

router.put('/:movie_id', (request,response) => {
    const{ movie_title, movie_release_date,movie_time,director_name} = request.body

    const query = `
    UPDATE movie
    SET
    movie_title = '${movie_title}',
    movie_release_date = '${movie_release_date}',
    movie_time = '${movie_time}',
    director_name = '${director_name}'   
    `

})


router.delete('/:movie_id', (request,response) => {
    const{ movie_title, movie_release_date,movie_time,director_name} = request.body

    const query = `
    DELETE FROM movie
    WHERE
    movie_id = '${movie_id}',
    `

})

module.exports = router