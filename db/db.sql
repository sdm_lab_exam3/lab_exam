CREATE TABLE movie (
    movie_id INTEGER PRIMARY KEY AUTO_INCREMENT,
    movie_title VARCHAR(50), 
    movie_release_date DATE,
    movie_time TIME,
    director_name VARCHAR(50)
);